from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Samuel Dimas Partogi Siahaan' 
curr_year = int(datetime.now().strftime("%Y"))
birth_date1 = date(1999, 4, 17)
npm = 1706074915
univ = 'Universitas Indonesia'
desc1 = 'I love singing, reading, and playing guitar.'
desc2 = 'I am currently studying in Universitas Indonesia, Faculty of Coumputer Science.'

def index(request):
    response = {'name': mhs_name, 
                'univ': univ, 
                'age': calculate_age(birth_date1.year), 
                'npm': npm, 
                'desc1': desc1, 
                'desc2': desc2}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
